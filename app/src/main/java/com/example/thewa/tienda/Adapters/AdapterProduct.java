package com.example.thewa.tienda.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.thewa.tienda.Models.Products;
import com.example.thewa.tienda.R;

import java.util.List;

public class AdapterProduct extends BaseAdapter {

    private static LayoutInflater inflater = null;
    private List<Products>  products;

    Context context;

    public AdapterProduct(Context context, List<Products> product){
        this.context = context;
        this.products = product;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        final View vista = inflater.inflate(R.layout.item_prod,null);
        TextView desc = vista.findViewById(R.id.Desc);
        TextView price = vista.findViewById(R.id.Price);
        ImageView image = vista.findViewById(R.id.ItemImage);
        desc.setText( products.get(i).getProd());
        price.setText(String.valueOf(products.get(i).getPrice()));

        return vista;
    }
}
