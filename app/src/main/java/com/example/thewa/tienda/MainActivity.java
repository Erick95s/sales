package com.example.thewa.tienda;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    private Button enterBTN,signUpBTN;
    private TextView emailTV,passwdTV;
    private FirebaseAuth Authenticator;
    private FirebaseAuth.AuthStateListener AuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InitFireBase();
        getContext();
        setEvents();
    }

    private void InitFireBase(){
        Authenticator = FirebaseAuth.getInstance();
        AuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser cUser = Authenticator.getCurrentUser();
                if(cUser == null){
                    Log.i("Firebase","No Logueado");
                }else{
                    Log.i("Firebase","Logueado");
                    //Toast.makeText(getApplicationContext(),cUser.getEmail(), Toast.LENGTH_LONG).show();
                    NavigateVenta();
                }
            }
        };
    }

    private void getContext(){
        signUpBTN = findViewById(R.id.LSignUp);
        enterBTN = findViewById(R.id.LLogin);
        emailTV = findViewById(R.id.LEmail);
        passwdTV = findViewById(R.id.LPassword);
    }

    private void setEvents(){
        signUpBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SignUp();
            }
        });
        enterBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Enter();
            }
        });
    }

    private void SignUp() {
        Log.e("dd","entra");
        Intent intent = new Intent(this,SignUp.class);
        startActivity(intent);
    }

    private void Out(){
        Authenticator.signOut();
    }
    private void NavigateVenta(){
        Intent intent = new Intent(this,Venta.class);
        Log.e("dd",Authenticator.getCurrentUser().getEmail());
        startActivity(intent);
    }
    private void Enter(){
        String email,pass;
        email = emailTV.getText().toString();
        pass = passwdTV.getText().toString();
        if(email.isEmpty() || pass.isEmpty()){
            Toast.makeText(getApplicationContext(),"Faltan Parametros", Toast.LENGTH_SHORT).show();
            return;
        }

        Authenticator.signInWithEmailAndPassword(email,pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(!task.isComplete()){
                    Toast.makeText(getApplicationContext(),"Revisa tu conexion a internet", Toast.LENGTH_LONG).show();
                    return;
                }
                if(!task.isSuccessful()){
                    passwdTV.setText("");
                    Toast.makeText(getApplicationContext(),"Usuario y/o contraseña incorrectos", Toast.LENGTH_LONG).show();
                    return;
                }

                Toast.makeText(getApplicationContext(),"entra", Toast.LENGTH_LONG).show();
                NavigateVenta();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Authenticator.addAuthStateListener(AuthListener);

    }

    @Override
    protected void onStop() {
        super.onStop();
        if(AuthListener != null){
            Authenticator.removeAuthStateListener(AuthListener);
        }
    }
}
