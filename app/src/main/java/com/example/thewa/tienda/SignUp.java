package com.example.thewa.tienda;

import android.content.Intent;
import android.graphics.drawable.PaintDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SignUp extends AppCompatActivity {

    private EditText name,email,pass,cpass;
    private Button register;
    private FirebaseAuth Authenticator;
    private FirebaseAuth.AuthStateListener AuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        InitFirebase();
        getContext();
        setEvents();

    }

    private void InitFirebase(){
        Authenticator = FirebaseAuth.getInstance();
        AuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser cUser = Authenticator.getCurrentUser();
                if(cUser == null){
                    Log.i("Firebase","No Logueado");
                }else{
                    Log.i("Firebase","Logueado");
                }
            }
        };
    }
    private void getContext(){
        name = findViewById(R.id.SGName);
        email = findViewById(R.id.SGMail);
        pass = findViewById(R.id.SGPass);
        cpass = findViewById(R.id.SGCPass);
        register = findViewById(R.id.SGSign);
    }

    private void setEvents(){
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sign();
            }
        });
    }

    private void NavigateLogin(){
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }
    private void Sign(){
        String Name,Email,Pass,Cpass;
        Name = name.getText().toString();
        Email = email.getText().toString();
        Pass = pass.getText().toString();
        Cpass = cpass.getText().toString();
        if(Name.isEmpty()|| Email.isEmpty() || Pass.isEmpty() || Cpass.isEmpty()){
            Toast.makeText(getApplicationContext(),"Los datos no estan completos", Toast.LENGTH_SHORT).show();
            return;
        }
        if(!Pass.equals(Cpass)){
            Toast.makeText(getApplicationContext(),"Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
            return;
        }
        Authenticator.createUserWithEmailAndPassword(Email,Pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(!task.isComplete()){
                    pass.setText("");
                    cpass.setText("");
                    Toast.makeText(getApplicationContext(),"Revisa tu conexion a internet", Toast.LENGTH_LONG).show();
                    return;
                }
                if(!task.isSuccessful()){
                    pass.setText("");
                    cpass.setText("");

                    Log.e("firebase",task.getException().toString());
                    Toast.makeText(getApplicationContext(),task.getException().toString(), Toast.LENGTH_LONG).show();
                    return;
                }
                Toast.makeText(getApplicationContext(),"Registro compleatdo", Toast.LENGTH_LONG).show();
                NavigateLogin();
            }
        });
    }
}
