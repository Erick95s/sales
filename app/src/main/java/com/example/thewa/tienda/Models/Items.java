package com.example.thewa.tienda.Models;

public class Items {
    private float cant;
    private String Prod;

    public  Items(){

    }

    public float getCant() {
        return cant;
    }

    public void setCant(float cant) {
        this.cant = cant;
    }

    public String getProd() {
        return Prod;
    }

    public void setProd(String prod) {
        Prod = prod;
    }
}
