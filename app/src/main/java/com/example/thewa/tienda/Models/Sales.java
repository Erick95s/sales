package com.example.thewa.tienda.Models;

import java.util.List;

public class Sales {
    private String date;
    private Float total;
    private List<Items> items;


    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    public List<Items> getItems() {
        return items;
    }

    public void setItems(List<Items> items) {
        this.items = items;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    @Override
    public String toString() {
        return String.valueOf(total);
    }
}
